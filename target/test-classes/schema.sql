create table Project4_Adressen(
                                  AdresID int identity ,
                                  Straat varchar(50),
                                  Nummer varchar(50),
                                  Postcode int,
                                  Gemeente varchar(20),
                                  primary key(AdresID));

create table Project4_Klanten(
                                 KlantID int identity ,
                                 AdresID int,
                                 Voornaam varchar(20),
                                 Achternaam varchar(20),
                                 Email varchar(50),
                                 Telefoon int,
                                 primary key(KlantID),
                                 foreign key(AdresID) references Project4_Adressen(AdresID));
create table Project4_Uitbaters(
                                   UitbaterID int identity ,
                                   Voornaam varchar(50),
                                   Achternaam varchar(50),
                                   primary key (UitbaterID));

create table Project4_Zaken(
                               ZaakID int identity ,
                               UitbaterID int,
                               AdresID int,
                               Bedrijfsnaam varchar(50),
                               Openingsuren varchar(50),
                               primary key(ZaakID),
                               foreign key(UitbaterID) references Project4_Uitbaters(UitbaterID),
                               foreign key(AdresID) references Project4_Adressen(AdresID));

create table Project4_MenuItems(

                                   MenuItemID int identity ,
                                   ZaakID int,
                                   MenuItemNaam varchar(50),
                                   eenheidsprijs float,
                                   primary key(MenuItemID),
                                   foreign key(ZaakID) references Project4_Zaken(ZaakID));
create table Project4_Menu_Zaak_Koppel(
                                          MenuZaakID int identity ,
                                          MenuItemID int,
                                          ZaakID int,
                                          primary key(MenuZaakID),
                                          foreign key(MenuItemID) references Project4_MenuItems(MenuItemID),
                                          foreign key(ZaakID) references Project4_Zaken(ZaakID));

create table Project4_Bezorgers(
                                   BezorgerID int identity ,
                                   Voornaam varchar(50),
                                   Vervoersmiddel varchar(50),
                                   primary key(BezorgerID));

create table Project4_Bestellingen(
                                      BestelID int identity ,
                                      KlantID int,
                                      BezorgerID int,
                                      ZaakID int,
                                      Datum date,
                                      Totaalprijs float,
                                      DatumLevering date,

                                      primary key(BestelID),
                                      foreign key(KlantID) references Project4_Klanten(KlantID),
                                      foreign key(BezorgerID) references Project4_Bezorgers(BezorgerID),
                                      foreign key(ZaakID) references Project4_Zaken(ZaakID));




create table Project4_VerkochteItems(
                                        ItemID int identity ,
                                        BestelID int,
                                        MenuItemID int,
                                        aantal int,
                                        verkoopprijs float,
                                        primary key(ItemID),
                                        foreign key(BestelID) references Project4_Bestellingen(BestelID),
                                        foreign key(MenuItemID) references Project4_MenuItems(MenuItemID) ON DELETE set null);











