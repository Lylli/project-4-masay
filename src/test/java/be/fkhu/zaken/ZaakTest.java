package be.fkhu.zaken;

import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ZaakTest {
    Zaak zaak = new Zaak ("EersteNaam");
    Adres adres = new Adres("Kleppersstraat", "13", 9000, "Gent");
    Collection<MenuItem> menu = new ArrayList<>();
    Zaak zaakMetAllePm = new Zaak("Tweede", adres, "open op alle weekdagen van 9u tot 22u", menu);

    @Test
    void testConstructorWithName(){
        String name = zaak.getNameZaak();
        Assertions.assertEquals("EersteNaam", name);

    }

    @Test
    void testConstructorWithAllPm(){
//        String nameZaak, Adres adres,
//                String openingsUren, Collection<MenuItem> menu
        String name = zaakMetAllePm.getNameZaak();
        Adres adres1 = zaakMetAllePm.getAdres();
        String opening= zaakMetAllePm.getOpeningsUren();
        Collection<MenuItem> menuItems = zaakMetAllePm.getMenu();

        Assertions.assertEquals("Tweede", name);
        Assertions.assertEquals(adres, adres1);
        Assertions.assertEquals("open op alle weekdagen van 9u tot 22u",opening);
        Assertions.assertEquals(menuItems, menu);
    }

    @Test
    void setOpeningsUren() {
        String newString = "alle weekdagen open 24/24";
        zaakMetAllePm.setOpeningsUren(newString);
        String result = zaakMetAllePm.getOpeningsUren();
        Assertions.assertEquals(newString, result);
    }

    @Test
    void setMenu() {
        MenuItem gerecht = new MenuItem("spaghetti", 5.00);
        menu.add(gerecht);
        zaak.setMenu(menu);
        Collection<MenuItem> result = zaak.getMenu();
        Collection<MenuItem> menuTest = new ArrayList<>();
        menuTest.add(gerecht);

        Assertions.assertEquals(menuTest, result);

    }

    @Test
    void setNameZaak() {
        zaak.setNameZaak("Een naam");
        String result = zaak.getNameZaak();
        Assertions.assertEquals("Een naam", result);
    }

    @Test
    void setAdres() {
        Adres newAdres = new Adres("Straat","0", 0000, "Brussel");
        zaak.setAdres(newAdres);
        Adres result = zaak.getAdres();
        Assertions.assertEquals(newAdres, result);
    }

    @Test
    void setUitbater() {
        Name nameUitbater= new Name("Alex", "Brown");
        Uitbater uitbater = new Uitbater(nameUitbater);
        zaak.setUitbater(uitbater);
        Uitbater result = zaak.getUitbater();
        Assertions.assertEquals(uitbater, result);
    }
}