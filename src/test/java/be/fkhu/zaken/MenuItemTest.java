package be.fkhu.zaken;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

class MenuItemTest {
    MenuItem gerecht = new MenuItem("Spaghetti", 6.00);
    Collection<Zaak> zaken = new ArrayList<>();
    Zaak zaak = new Zaak("Burgerzaak");

    @Test
    void testConstructor(){
        String name = gerecht.getName();
        double price = gerecht.getPrice();
        Assertions.assertEquals("Spaghetti", name);
        Assertions.assertEquals(6.00, price);

    }

    @Test
    void setName() {
        gerecht.setName("Tagliatteli");
        String result = gerecht.getName();
        Assertions.assertEquals("Tagliatteli", result);
    }

    @Test
    void setPrice() {
        gerecht.setPrice(20.00);
        double result = gerecht.getPrice();
        Assertions.assertEquals(20.00, result);
    }




}