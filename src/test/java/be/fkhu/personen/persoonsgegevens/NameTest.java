package be.fkhu.personen.persoonsgegevens;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NameTest {
        Name name = new Name ("Natalie", "Heylen");

    @Test
    void testConstructor(){
        String firstName = name.getFirstName();
        String lastName = name.getLastName();
        Assertions.assertEquals("Natalie", firstName);
        Assertions.assertEquals("Heylen", lastName);
    }

    @Test
    void setFirstName() {
        name.setFirstName("Lylli");
        String result = name.getFirstName();
        Assertions.assertEquals("Lylli", result);

    }

    @Test
    void setLastName() {
        name.setLastName("Luinedhlien");
        String result = name.getLastName();
        Assertions.assertEquals("Luinedhlien", result);
    }
}