package be.fkhu.personen;

import be.fkhu.orders.Bestelling;
import be.fkhu.personen.persoonsgegevens.Name;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BezorgerTest {
    Bezorger bezorger= new Bezorger("Natalie");
    Klant klant = new Klant(new Name("Natalie"));
    Bestelling bestelling = new Bestelling(klant);

    @Test
    void setName() {
        bezorger.setName("Lylli");
        String result = bezorger.getName();
        Assertions.assertEquals("Lylli", result);

    }

    @Test
    void setVervoersmiddel() {
        bezorger.setVervoersmiddel("auto");
        String result = bezorger.getVervoersmiddel();
        Assertions.assertEquals("auto", result);
    }

    @Test
    void setBestelling() {
        bezorger.setBestelling(bestelling);
        Bestelling result = bezorger.getBestelling();
        Assertions.assertEquals(bestelling, result);
    }
}