package be.fkhu.personen;

import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.zaken.Zaak;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class UitbaterTest {
    Collection<Zaak> zaken = new ArrayList<>();
    Zaak zaak = new Zaak("NaamZaak");
    Name name = new Name("Voornaam", "Achternaam");
    Uitbater uitbater= new Uitbater(name, zaken);

    @Test
    void testConstructorNaam(){
        Name nameUitbater = uitbater.getName();
        Assertions.assertEquals(name, nameUitbater);
    }

    @Test
    void testConstructorAllePm(){
        Name nameUitbater = uitbater.getName();
        Collection<Zaak> zaakCollection = uitbater.getZaken();
        Assertions.assertEquals(name, nameUitbater);
        Assertions.assertEquals(zaakCollection, zaken);

    }

    @Test
    void setName() {
        Name newName = new Name ("Nieuwe", "Naam");
        uitbater.setName(newName);
        Name result = uitbater.getName();
        Assertions.assertEquals(newName, result);
    }

    @Test
    void setZaken() {
        zaken.add(zaak);
        uitbater.setZaken(zaken);
        Collection<Zaak> result = uitbater.getZaken();

        Collection<Zaak> test = new ArrayList<>();
        test.add(zaak);

        Assertions.assertEquals(test, result);
    }
}