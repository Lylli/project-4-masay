package be.fkhu.personen;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class KlantTest {
    Adres adres = new Adres("Kleppersstraat", "13", 9000, "Gent");
    Klant klant = new Klant();
    Name name = new Name("Natalie");
    Collection<Bestelling> bestellingen = new ArrayList<>();
    Collection<VerkochtItem> verkochtItems = new ArrayList<>();

    Bestelling bestelling = new Bestelling(verkochtItems);


    @Test
    void setAdres() {
        klant.setAdres(adres);
        Adres result = klant.getAdres();
        Assertions.assertEquals(adres, result);
    }

    @Test
    void setName() {
        klant.setName(name);
        Name result = klant.getName();
        Assertions.assertEquals(name, result);
    }

    @Test
    void setBestellingen() {
        verkochtItems.add(new VerkochtItem(new MenuItem("spaghetti", 5.00),2));
        bestellingen.add(bestelling);
        klant.setBestellingen(bestellingen);
        Collection<Bestelling> result = klant.getBestellingen();

        Collection<Bestelling> test = new ArrayList<>();
        test.add(bestelling);

        Assertions.assertEquals(test, result);
    }
}