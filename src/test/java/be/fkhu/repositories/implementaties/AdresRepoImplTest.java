package be.fkhu.repositories.implementaties;

import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.repositories.AdresRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class AdresRepoImplTest {

    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }


    @Test
    void saveAdress(){
        facade = new JPAFacade("test");
        Adres adres = new Adres("Martelaarslaan", "2", 9000, "Gent");
        AdresRepo adresRepo = new AdresRepoImpl(facade);
        adresRepo.saveAdres(adres);
        Adres result = adresRepo.findAdres(0);
        System.out.println(adres);
        System.out.println(result);
        Assertions.assertEquals(adres, result);
    }

}