package be.fkhu.repositories.implementaties;

import be.fkhu.personen.Klant;
import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.repositories.KlantRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.Zaak;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class KlantRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }
//persist
    @Test
    void saveKlant() {
        facade = new JPAFacade("test");
        Klant klant = new Klant(new Name("Keifancy naam"));
        KlantRepo klantRepo = new KlantRepoImpl(facade);
        klantRepo.saveKlant(klant);

        Klant result = klantRepo.findKlant(0);
        System.out.println(klant);
        System.out.println(result);
        Assertions.assertEquals(klant, result);
    }
//merge
    @Test
    void updateKlant() {
        facade = new JPAFacade("test");
        Klant klant = new Klant(new Name("Keifancy naam"));
        Adres adres = new Adres("Kleppersstraat", "7", 9000, "Gent");
        klant.setAdres(adres);
        KlantRepo klantRepo = new KlantRepoImpl(facade);
        klantRepo.updateKlant(klant);

        Klant result = klantRepo.findKlant(0);
        System.out.println(klant);
        System.out.println(result);
        Assertions.assertEquals(klant, result);
    }

//    TODO: is mislukt, maar dat komt omdat de adresid niet overeenkomen
    @Test
    void showAllZaken() {
        Adres adres1 = new Adres("Martelaarslaan", "2", 9000, "Gent");
        Zaak zaak1= new Zaak("McRonald’s", adres1);
        Adres adres2 = new Adres("Ottergemsesteenweg", "13", 2000, "Antwerpen");
        Zaak zaak2 = new Zaak ("Osaka Japanese Steak & Sushi", adres2);
        Adres adres3 = new Adres("Fochplein", "890", 3000, "Leuven");
        Zaak zaak3 = new Zaak ("Bolelli’s", adres3);
        Adres adres4 = new Adres("wdfbwd", "fdwbwd", 2500, "wdfbw");
        Zaak zaak4 = new Zaak("dfqrgbvqw", adres4);
        zaak4.setOpeningsUren("wdfbwdf");

        Collection<Zaak> zakenTest = new ArrayList<>();
        zakenTest.add(zaak1);
        zakenTest.add(zaak2);
        zakenTest.add(zaak3);
        zakenTest.add(zaak4);


        KlantRepo klantRepo = new KlantRepoImpl(facade);
        List<Zaak> test = klantRepo.showAllZaken("findAllZaken");
        Assertions.assertEquals(zakenTest, test);
    }
}