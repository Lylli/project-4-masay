package be.fkhu.repositories.implementaties;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.Bezorger;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.repositories.*;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.NamedQuery;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BezorgerRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }
    @Test
    void saveBezorger() {
        facade = new JPAFacade("test");
        Bezorger bezorger = new Bezorger("Natalie", "auto");
        BezorgerRepo bezorgerRepo = new BezorgerRepoImpl(facade);
        bezorgerRepo.saveBezorger(bezorger);
        Bezorger result = bezorgerRepo.findBezorger(0);
        System.out.println(bezorger);
        System.out.println(result);
        Assertions.assertEquals(bezorger, result);
    }

//    TODO: test niet geslaagd waarschijnlijk door de enumeratie status van bestelling
    @Test
    void showAllBestellingen() {
        facade = new JPAFacade("test");
        MenuItem menuItem = new MenuItem("gerecht1", 5.00);
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl(facade);
        MenuItem menuItem1 = new MenuItem("gerecht2", 6.00);
        menuItemRepo.saveGerecht(menuItem);
        menuItemRepo.saveGerecht(menuItem1);

        VerkochtItem verkochtItem = new VerkochtItem(menuItem,1);
        VerkochtItem verkochtItem1 = new VerkochtItem(menuItem1, 2);
        VerkochtItemRepo verkochtItemRepo = new VerkochtItemRepoImpl(facade);
        verkochtItemRepo.saveVerkochtItem(verkochtItem);
        verkochtItemRepo.saveVerkochtItem(verkochtItem1);

        Collection<VerkochtItem> bestelling = new ArrayList<>();
        bestelling.add(verkochtItem);
        bestelling.add(verkochtItem1);

        Bestelling bestelling1= new Bestelling(bestelling);

        Collection<VerkochtItem> bestellingCollection2 = new ArrayList<>();
        bestellingCollection2.add(verkochtItem);
        bestellingCollection2.add(verkochtItem1);

        Bestelling bestelling2 = new Bestelling(bestellingCollection2);

        BestellingRepo bestellingRepo = new BestellingRepoImpl(facade);
        bestellingRepo.saveBestelling(bestelling1);
        bestellingRepo.saveBestelling(bestelling2);


        Collection<Bestelling> bestellingen = new ArrayList<>();
        bestellingen.add(bestelling1);
        bestellingen.add(bestelling2);



        BezorgerRepo bezorgerRepo = new BezorgerRepoImpl(facade);

        List<Bestelling> test = bezorgerRepo.showAllBestellingen("alleBestellingen");
        List<Bestelling>testACCEPTED  = bezorgerRepo.showAllBestellingen("alleAccepteBestellingen");
        System.out.println(bestellingen);
        System.out.println(test);
        Assertions.assertEquals(bestellingen, test);
        Assertions.assertEquals(bestellingen, testACCEPTED);


    }

}