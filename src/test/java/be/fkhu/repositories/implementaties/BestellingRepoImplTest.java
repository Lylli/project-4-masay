package be.fkhu.repositories.implementaties;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.Klant;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.repositories.BestellingRepo;
import be.fkhu.repositories.KlantRepo;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.repositories.VerkochtItemRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.MenuItem;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class BestellingRepoImplTest {
    JPAFacade facade;
    Collection<VerkochtItem> verkochtItemCollection = new ArrayList<>();
    Klant klant;
    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }


    @Test
    void saveBestelling() {
        facade = new JPAFacade("test");
        klant = new Klant(new Name ("Lylli"));
        KlantRepo klantRepo = new KlantRepoImpl(facade);
        klantRepo.saveKlant(klant);
        Bestelling bestelling = new Bestelling(klant);
        bestelling.setTotalPrice(10);
        BestellingRepo bestellingRepo = new BestellingRepoImpl(facade);
        bestellingRepo.saveBestelling(bestelling);


        Bestelling result = bestellingRepo.findBestelling(0);
//        System.out.println(bestelling);
//        System.out.println(result);
        Assertions.assertEquals(bestelling, result);
    }
//TODO: failed: wrs omda de save niet werkt van menuitem
    @Test
    void addVerkochtItem() {
        facade = new JPAFacade("test");
        Bestelling bestelling = new Bestelling();
        MenuItem menuItem = new MenuItem("spag", 5.00);
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl(facade);
        menuItemRepo.saveGerecht(menuItem);
        VerkochtItem verkochtItem = new VerkochtItem(menuItem, 1);
        VerkochtItemRepo verkochtItemRepo = new VerkochtItemRepoImpl(facade);
        verkochtItemRepo.saveVerkochtItem(verkochtItem);
        BestellingRepo bestellingRepo = new BestellingRepoImpl(facade);

        Collection<VerkochtItem> testBestelling = new ArrayList<>();
        testBestelling.add(verkochtItem);
        bestellingRepo.addVerkochtItem(bestelling.getId(), verkochtItem);

        Bestelling result = bestellingRepo.findBestelling(bestelling.getId());
        Collection<VerkochtItem> resultBestelling = result.getVerkochteItems();
        System.out.println(testBestelling);
        System.out.println(resultBestelling);
        Assertions.assertEquals(testBestelling, resultBestelling);
    }
}