package be.fkhu.repositories.implementaties;

import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class MenuItemRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }
//TODO: result is null --> WAAROM?
    @Test
    void saveGerecht() {
        facade = new JPAFacade("test");
        MenuItem menuItem = new MenuItem("linguini",5.00);
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl(facade);
        menuItemRepo.saveGerecht(menuItem);

        MenuItem result = menuItemRepo.findGerecht(1);
        System.out.println(menuItem);
        System.out.println(result);
        Assertions.assertEquals(menuItem, result);
    }
//TODO nullpointerexception
    @Test
    void addGerecht() {
        facade = new JPAFacade("test");
        Zaak zaak= new Zaak("Fancy zaak");
        ZaakRepoImpl zaakRepo = new ZaakRepoImpl(facade);
        zaakRepo.saveZaak(zaak);
        MenuItem gerecht = new MenuItem("linguini",5.00);
        zaak = zaakRepo.findZaak(zaak.getId());
        gerecht.setZaak(zaak);


        zaak.getMenu().add(gerecht);
        zaak.setMenu(zaak.getMenu());
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl(facade);
        menuItemRepo.addGerecht(zaak.getId(), gerecht);

        MenuItem result = menuItemRepo.findGerecht(gerecht.getId());
        System.out.println(gerecht);
        System.out.println(result);
        Assertions.assertEquals(gerecht, result);

    }

    @Test
    void removeGerecht() {
        MenuItem gerecht = new MenuItem("linguini",5.00);
//        gerecht = facade.find(MenuItem.class, gerecht.getId());
//
//        facade.getTransaction().begin();
//        facade.getManager().remove(gerecht);
//        facade.getTransaction().commit();
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl(facade);
        menuItemRepo.saveGerecht(gerecht);
        menuItemRepo.removeGerecht(26);

        long id = gerecht.getId();
        MenuItem result = menuItemRepo.findGerecht(26);
        System.out.println(result);


//        MenuItem result = facade.find(MenuItem.class, gerecht.getId());
//TODO: ik kreeg hier een nullpointerexception, dus wel wat ik verwachtte, maar toch nog steeds test failed
        Assertions.assertThrows(NullPointerException.class, () -> menuItemRepo.removeGerecht(26));
//        Assertions.assertNull(result);
    }
}