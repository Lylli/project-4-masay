package be.fkhu.repositories.implementaties;

import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.repositories.VerkochtItemRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.MenuItem;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class VerkochtItemRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }
//TODO: failed
    @Test
    void saveVerkochtItem() {
        facade = new JPAFacade("test");
        MenuItem menuItem = new MenuItem("lasagna", 5.00);
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl();
        menuItemRepo.saveGerecht(menuItem);
        VerkochtItem verkochtItem = new VerkochtItem(menuItem, 2);
        VerkochtItemRepo verkochtItemRepo = new VerkochtItemRepoImpl(facade);
        verkochtItemRepo.saveVerkochtItem(verkochtItem);
        VerkochtItem result = verkochtItemRepo.findVerkochtItem(verkochtItem.getId());
//        VerkochtItem result = facade.find(VerkochtItem.class, verkochtItem.getId());
        Assertions.assertEquals(verkochtItem, result);
    }
}