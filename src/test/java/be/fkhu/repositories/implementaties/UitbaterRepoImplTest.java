package be.fkhu.repositories.implementaties;

import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.repositories.UitbaterRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.Zaak;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class UitbaterRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }

    @Test
    void saveUitbater() {
        facade = new JPAFacade("test");
        Uitbater uitbater = new Uitbater(new Name("Keifancy naam"));
        facade.persist(uitbater);
        Uitbater result = facade.find(Uitbater.class, uitbater.getId());
        Assertions.assertEquals(uitbater, result);
    }
//TODO: failed
    @Test
    void addZaak() {
        facade = new JPAFacade("test");
        UitbaterRepo uitbaterRepo = new UitbaterRepoImpl(facade);
        Uitbater uitbater = uitbaterRepo.findUitbater(1);
        Zaak zaak = new Zaak ("een naam");
        zaak.setUitbater(uitbater);
        ZaakRepo zaakRepo = new ZaakRepoImpl();
        zaakRepo.saveZaak(zaak);
        uitbater.getZaken().add(zaak);


        Collection<Zaak> zaken= new ArrayList<>();
        zaken.add(zaak);


        uitbaterRepo.addZaak(1, zaak);

        Uitbater result = uitbaterRepo.findUitbater(1);
        Collection<Zaak> zakenResult = result.getZaken();
        Assertions.assertEquals(zaken, zakenResult);

    }
// TODO: fail nullpointerexception
    @Test
    void removeZaak() {
        facade = new JPAFacade("test");
        UitbaterRepo uitbaterRepo = new UitbaterRepoImpl(facade);
        Uitbater uitbater = uitbaterRepo.findUitbater(1);
        Zaak zaak = new Zaak ("een naam");
        zaak.setUitbater(uitbater);
        ZaakRepo zaakRepo = new ZaakRepoImpl();
        zaakRepo.saveZaak(zaak);
        uitbater.getZaken().add(zaak);
        Collection<Zaak> zaken= new ArrayList<>();
        uitbaterRepo.removeZaak(1, zaak.getMenu());

        Uitbater result = uitbaterRepo.findUitbater(1);
        Collection<Zaak> zakenResult = result.getZaken();
        Assertions.assertEquals(zaken, zakenResult);
    }
}