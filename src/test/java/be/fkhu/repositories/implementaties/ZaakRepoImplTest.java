package be.fkhu.repositories.implementaties;

import be.fkhu.orders.VerkochtItem;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class ZaakRepoImplTest {
    JPAFacade facade;

    @BeforeEach
    public void init() throws SQLException, Exception {
        try (
                Connection con = DriverManager.getConnection("jdbc:hsqldb:mem:mymemdb", "sa", "");
                Statement statement = con.createStatement();
                Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
                Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }
    }

    @Test
    void saveZaak() {
        facade = new JPAFacade("test");
        Zaak zaak = new Zaak("Keifancy naam");
        ZaakRepo zaakRepo = new ZaakRepoImpl(facade);
        zaakRepo.saveZaak(zaak);
        Zaak result = zaakRepo.findZaak(zaak.getId());
        System.out.println(zaak);
        System.out.println(result);
        Assertions.assertEquals(zaak, result);
    }

//    TODO: bij merges krijg ik null
    @Test
    void mergeZaak() {
        facade = new JPAFacade("test");
        Zaak zaak = new Zaak("andere naam");
        ZaakRepo zaakRepo = new ZaakRepoImpl(facade);
        zaakRepo.mergeZaak(zaak);
        Zaak result = zaakRepo.findZaak(0);
        System.out.println(zaak);
        System.out.println(result);
        Assertions.assertEquals(zaak, result);

    }

    @Test
    void findZaak(){
        facade= new JPAFacade("test");
        ZaakRepo zaakRepo = new ZaakRepoImpl(facade);
        Zaak result = zaakRepo.findZaak(1);
        System.out.println(result);


    }
}