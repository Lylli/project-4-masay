package be.fkhu.orders;

import be.fkhu.personen.Klant;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.zaken.MenuItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerkochtItemTest {
    MenuItem gerecht = new MenuItem("spaghett", 5.00);
    Klant klant = new Klant(new Name("Natalie"));
    Bestelling bestelling = new Bestelling(klant);
    VerkochtItem verkochtItem = new VerkochtItem(gerecht, 2);

    @Test
    void setGerecht() {
        verkochtItem.setGerecht(gerecht);
        MenuItem result = verkochtItem.getGerecht();
        Assertions.assertEquals(gerecht, result);
    }

    @Test
    void setBestelling() {
        verkochtItem.setBestelling(bestelling);
        Bestelling result = verkochtItem.getBestelling();
        Assertions.assertEquals(bestelling, result);
    }

    @Test
    void setNumber() {
        verkochtItem.setNumber(5);
        int result = verkochtItem.getNumber();
        Assertions.assertEquals(5, result);
    }

    @Test
    void setPrice() {

        verkochtItem.setPrice(gerecht.getPrice()* verkochtItem.getNumber());
        double result = verkochtItem.getPrice();
        Assertions.assertEquals(10.00, result);
    }
}