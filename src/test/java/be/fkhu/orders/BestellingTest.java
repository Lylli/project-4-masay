package be.fkhu.orders;

import be.fkhu.personen.Bezorger;
import be.fkhu.personen.Klant;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class BestellingTest {
    Klant klant = new Klant (new Name("Natalie"));
    Bestelling bestelling = new Bestelling();
    Collection<VerkochtItem> verkochtItemCollection = new ArrayList<>();
    VerkochtItem verkochtItem= new VerkochtItem(new MenuItem("spaghetti", 5.00), 5);

    @Test
    void setKlant() {
        bestelling.setKlant(klant);
        Klant result = bestelling.getKlant();
        Assertions.assertEquals(klant, result);
    }

    @Test
    void setVerkochteItems() {
        verkochtItemCollection.add(verkochtItem);
        bestelling.setVerkochteItems(verkochtItemCollection);
        Collection<VerkochtItem> result = bestelling.getVerkochteItems();

        Collection<VerkochtItem> test = new ArrayList<>();
        test.add(verkochtItem);

        Assertions.assertEquals(test, result);
    }

    @Test
    void setTotalPrice() {
        bestelling.setTotalPrice(25.00);
        double result = bestelling.getTotalPrice();
        Assertions.assertEquals(25.00, result);
    }

    @Test
    void setStatus() {
        bestelling.setStatus(Status.ACCEPTED);
        Status result = bestelling.getStatus();
        Assertions.assertEquals(Status.ACCEPTED, result);
    }

    @Test
    void setBezorger() {
        Bezorger bezorger = new Bezorger("Lylli");
        bestelling.setBezorger(bezorger);
        Bezorger result = bestelling.getBezorger();
        Assertions.assertEquals(bezorger, result);
    }

    @Test
    void setZaak() {
        Zaak zaak = new Zaak("Testing");
        bestelling.setZaak(zaak);
        Zaak result = bestelling.getZaak();
        Assertions.assertEquals(zaak, result);
    }
}