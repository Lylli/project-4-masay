insert into Project4_Adressen (Straat, Nummer, Postcode, Gemeente)
values ('Martelaarslaan',2, 9000, 'Gent'),
       ('Ottergemsesteenweg', 13, 2000,'Antwerpen'),
       ('Fochplein', 890, 3000,'Leuven');

insert into Project4_MenuItems (MenuItemNaam, eenheidsprijs)
values ('Substantial Mac', 6.35),
       ('Royal Cheese', 7.10),
       ('Rooster McNuggets', 6.85),
       ('Filet-o-Ghoti', 6.35),
       ('0.25th Pounder', 6.35),
       ('Sapporo', 4.00),
       ('Kirin', 5.00),
       ('Sashimi no moriawase', 40.00),
       ('Sushi (8 stuks)', 35.00),
       ('Tempura Maki', 25.00),
       ('Veggie Tempura Maku', 20.00),
       ('Te maki set', 40.00),
       ('Nice Tea', 2.99),
       ('Tomatensoep', 8.00),
       ('Groentensoep', 9.50),
       ('Focaccia met oregano', 8.00),
       ('Scampis gefrituurd met een huisgemaakte tartaarsaus', 16.00),
       ('Caesar Salade met een Italiaanse tint', 15.50),
       ('Gegrilde tonijnsteak', 24.00),
       ('Kalfschenkel met tomatensaus en groenten', 22.00),
       ('Tagliatelle Bolognese', 17.00),
       ('Penne Arrabiata', 16.00),
       ('Spaghetti Carbonara', 18.00),
       ('Linguine met Venusschelpen', 8.00),
       ('Lasagna', 8.00);

insert into Project4_Uitbaters (Voornaam, Achternaam)
values ('Ronald','Weasely'), ('Satoshi','Tajiri'), ('Octaaf','Braaf');

insert into Project4_Zaken (UitbaterID, AdresID, Bedrijfsnaam)
values (1, 1, 'McRonald’s'), (2,2,'Osaka Japanese Steak & Sushi'), (3,3,'Bolelli’s');

/*menu items 1 tot en met 5: ZaakID = 1
menu items 6 tot en met 12: zaakId = 2
menuitems 13 tot en met 25: zaakId =3*/
insert into Project4_Menu_Zaak_Koppel (MenuItemID, ZaakID)
values (1,1),
       (2,1),
       (3,1),
       (4,1),
       (5,1),
       (6,2),
       (7,2),
       (8,2),
       (9,2),
       (10,2),
       (11,2),
       (12,2),
       (13,3),
       (14,3),
       (15,3),
       (16,3),
       (17,3),
       (18,3),
       (19,3),
       (20,3),
       (21,3),
       (22,3),
       (23,3),
       (24,3),
       (25,3);



alter table Project4_Bestellingen
    add column Status varchar(50);


alter table Project4_Bestellingen
    alter column Status ENUM('ACCEPTED', 'NOT_ACCEPTED', 'GELEVERD');


insert into Project4_Bezorgers (Voornaam, Vervoersmiddel)
values ('Natalie', 'auto');


alter table Project4_Zaken
--     add column Status ('ACTIEF', 'NON_ACTIEF') default 'ACTIEF' ;
add column Status varchar(50) not null unique ;

insert into Project4_Zaken
values ('ACTIEF'), ('NON_ACTIEF');






