package be.fkhu.orders;

import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */


@Entity
@Table(name = "Project4_VerkochteItems")
public class VerkochtItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ItemID")
    private long id;

    @OneToOne (cascade = CascadeType.MERGE)
    @JoinColumn(name = "MenuItemID")
    private MenuItem gerecht;

    @ManyToOne (cascade = CascadeType.PERSIST)
    @JoinColumn(name ="BestelID")
    private Bestelling bestelling;

    @Column(name = "aantal")
    private int number;

    @Column (name = "verkoopprijs")
    private double price;



    public VerkochtItem() {
    }

    public VerkochtItem(MenuItem gerecht, int number) {
        this.gerecht = gerecht;
        this.number = number;
    }

    public VerkochtItem(MenuItem gerecht, int number, double price) {
        this.gerecht = gerecht;
        this.number = number;
        this.price = price;
    }

    public VerkochtItem(MenuItem gerecht, Bestelling bestelling, int number) {
        this.gerecht = gerecht;
        this.bestelling = bestelling;
        this.number = number;
    }

    public VerkochtItem(MenuItem gerecht, Bestelling bestelling, int number, double price) {
        this.gerecht = gerecht;
        this.bestelling = bestelling;
        this.number = number;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public MenuItem getGerecht() {
        return gerecht;
    }

    public void setGerecht(MenuItem gerecht) {
        this.gerecht = gerecht;
    }

    public Bestelling getBestelling() {
        return bestelling;
    }

    public void setBestelling(Bestelling bestelling) {
        this.bestelling = bestelling;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        price = gerecht.getPrice() * number;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VerkochtItem that = (VerkochtItem) o;
        return number == that.number && Double.compare(that.price, price) == 0 && Objects.equals(gerecht, that.gerecht) &&
                Objects.equals(bestelling, that.bestelling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gerecht, bestelling, number, price);
    }

    @Override
    public String toString() {
        return "VerkochtItem{" +
                "id=" + id +
                ", gerecht=" + gerecht +
                ", bestelling=" + bestelling +
                ", number=" + number +
                ", price=" + price +
                '}';
    }
}
