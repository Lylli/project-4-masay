package be.fkhu.orders;

import be.fkhu.personen.Bezorger;
import be.fkhu.personen.Klant;
import be.fkhu.zaken.Zaak;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */

@Entity
@Table(name = "Project4_Bestellingen")
@NamedQuery(name = "alleBestellingen", query = "select b from Bestelling b where b.status = be.fkhu.orders.Status.NOT_ACCEPTED")
@NamedQuery(name = "alleAccepteBestellingen", query = "select b from Bestelling b where b.status = be.fkhu.orders.Status.ACCEPTED")
public class Bestelling {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BestelID")
    private long id;

    @JoinColumn(name = "KlantID")
    @ManyToOne(cascade = CascadeType.MERGE)
    private Klant klant;

    @Column(name = "Datum")
    private LocalDateTime orderDateTime;

    @OneToMany (mappedBy = "bestelling", cascade = CascadeType.PERSIST)
    Collection<VerkochtItem> verkochteItems;

    @Column (name= "Totaalprijs")
    private double totalPrice;

    @Column (name = "Status", columnDefinition = "ENUM('ACCEPTED','NOT_ACCEPTED', 'GELEVERD')")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column (name = "DatumLevering")
    private LocalDateTime dateLevering;

    @OneToOne
    @JoinColumn(name = "BezorgerID")
    private Bezorger bezorger;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ZaakID")
    private Zaak zaak;

    public Bestelling() {
        setStatus(Status.NOT_ACCEPTED);
    }

    public Bestelling(Klant klant) {
        this.klant = klant;
        setStatus(Status.NOT_ACCEPTED);
    }

    public Bestelling(Collection<VerkochtItem> verkochteItems) {
        this.verkochteItems = verkochteItems;
    }

    public Bestelling(Klant klant, Collection<VerkochtItem> verkochteItems, double totalPrice) {
        this.klant = klant;
        this.verkochteItems = verkochteItems;
        this.totalPrice = totalPrice;
        setStatus(Status.NOT_ACCEPTED);
    }

    public Bestelling(Klant klant, LocalDateTime orderDateTime, Collection<VerkochtItem> verkochteItems, double totalPrice, Status status) {
        this.klant = klant;
        this.orderDateTime = orderDateTime;
        this.verkochteItems = verkochteItems;
        this.totalPrice = totalPrice;
        setStatus(Status.NOT_ACCEPTED);
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public Klant getKlant() {
        return klant;
    }

    public void setKlant(Klant klant) {
        this.klant = klant;
    }

    public LocalDateTime getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(LocalDateTime orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public Collection<VerkochtItem> getVerkochteItems() {
        return verkochteItems;
    }

    public void setVerkochteItems(Collection<VerkochtItem> verkochteItems) {
        this.verkochteItems = verkochteItems;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getDateLevering() {
        return dateLevering;
    }

    public void setDateLevering(LocalDateTime dateLevering) {
        this.dateLevering = dateLevering;
    }

    public Bezorger getBezorger() {
        return bezorger;
    }

    public void setBezorger(Bezorger bezorger) {
        this.bezorger = bezorger;
    }

    public Zaak getZaak() {
        return zaak;
    }

    public void setZaak(Zaak zaak) {
        this.zaak = zaak;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bestelling that = (Bestelling) o;
        return Double.compare(that.totalPrice, totalPrice) == 0 && Objects.equals(klant, that.klant) &&
                 status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(klant, totalPrice, status);
    }

    @Override
    public String toString() {
        return "Bestelling{" +
                "id=" + id +
                ", klant=" + klant.getName() +
                ", orderDateTime=" + orderDateTime +
                ", verkochteItems=" + verkochteItems.toArray().length +
                ", totalPrice=" + totalPrice +
                ", status='" + status + '\'' +
                ", dateLevering=" + dateLevering +
                '}';
    }
}
