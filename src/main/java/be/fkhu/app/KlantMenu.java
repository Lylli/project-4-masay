package be.fkhu.app;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.Klant;
import be.fkhu.repositories.BestellingRepo;
import be.fkhu.repositories.KlantRepo;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.repositories.implementaties.BestellingRepoImpl;
import be.fkhu.repositories.implementaties.KlantRepoImpl;
import be.fkhu.repositories.implementaties.MenuItemRepoImpl;
import be.fkhu.repositories.implementaties.ZaakRepoImpl;
import be.fkhu.utilities.input.Input;
import be.fkhu.utilities.input.InputKlantMenu;
import be.fkhu.utilities.output.Keyboard;
import be.fkhu.utilities.output.LayOut;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;


/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class KlantMenu {
    private static Klant klant;
    private static Keyboard keyboard;
    private static int choiceKlant;
    private static long klantenNummer;
    private static Collection <Bestelling> alleBestellingenVanDezeKlant;



    public static void startKlantMenu(){
        zaakOfBestellingKeuzeMenu();
    }

    public static void zaakOfBestellingKeuzeMenu(){
        long id = bestaandeOfNieuweKlant();
        KlantRepo klantRepo = new KlantRepoImpl();
        klant = klantRepo.findKlant(id);
        InputKlantMenu input= new InputKlantMenu();
        Bestelling bestelling = new Bestelling(klant);
        bestelling.setKlant(klant);
        Collection<VerkochtItem> besteldeItems = new ArrayList<>();

        choiceKlant = input.getChoiceZaakOfBestellingKeuzeMenu();
        while (choiceKlant != 3){
            if (choiceKlant == 1){
               long choiceZaak = overzichtZaken();
               long choiceGerecht = overzichtMenuVanGekozenZaak(choiceZaak);
               VerkochtItem gerechtDatBesteldIs = bestellingPlaatsen(choiceGerecht);
               besteldeItems.add(gerechtDatBesteldIs);
               gerechtDatBesteldIs.setBestelling(bestelling);

               System.out.println("Dit is voorlopig uw bestelling: ");
               besteldeItems.forEach(besteldItem -> System.out.println("Gerecht: " +
                       besteldItem.getGerecht() + " - Aantal: "+ besteldItem.getNumber() +
                       " - Totaalprijs van deze bestelling: " + besteldItem.getPrice()));

               int choiceMeerBestellen = input.meerBestellen();
               if (choiceMeerBestellen == 1){
                   choiceKlant = 1;

               }else{

                   bestelling.setVerkochteItems(besteldeItems);

                   double totalPrice = besteldeItems.stream()
                                                    .mapToDouble(besteldItem -> besteldItem.getPrice())
                                                    .sum();

                   bestelling.setTotalPrice(totalPrice);
                   bestelling.setOrderDateTime(LocalDateTime.now());
                   ZaakRepo zaakRepo = new ZaakRepoImpl();
                   Zaak gekozenZaak = zaakRepo.findZaak(choiceZaak);
                   bestelling.setZaak(gekozenZaak);
                   BestellingRepo bestellingRepo = new BestellingRepoImpl();

                   if (alleBestellingenVanDezeKlant == null){
                       alleBestellingenVanDezeKlant = new ArrayList<>();
                   }
                   alleBestellingenVanDezeKlant.add(bestelling);
                   klant.setBestellingen(alleBestellingenVanDezeKlant);
                   bestellingRepo.saveBestelling(bestelling);
                   System.out.println(bestelling);


                   choiceKlant = input.getChoiceZaakOfBestellingKeuzeMenu();}

            }
            else{

                bestellingenZien();
                choiceKlant = input.getChoiceZaakOfBestellingKeuzeMenu();

            }
            System.out.println("KlantenMenu laden...");
        }
    }

    private static void bestellingenZien() {
        alleBestellingenVanDezeKlant = klant.getBestellingen();
        alleBestellingenVanDezeKlant.forEach(bestelling1 -> System.out.println("Bestelnummer: " + bestelling1.getId()+
                 " -Datum van de bestelling: "+ bestelling1.getOrderDateTime() +" -Totale prijs: " + bestelling1.getTotalPrice()+
                " -Status: " + bestelling1.getStatus() + " -Datum levering: " + bestelling1.getDateLevering()));
    }

    public static VerkochtItem bestellingPlaatsen(long choiceGerecht){
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl();
        MenuItem gerecht = menuItemRepo.findGerecht(choiceGerecht);
        int aantal = keyboard.readInt("Geef het aantal keer in dat u het gerecht wenst te bestellen:");
        VerkochtItem gerechtInDeBestelling =  new VerkochtItem(gerecht, aantal, gerecht.getPrice());
        return gerechtInDeBestelling;
    }

    public static long overzichtZaken(){
        KlantRepo klantRepo = new KlantRepoImpl();
        klantRepo.showAllZaken("findAllZaken");
        long choice = keyboard.readLong("Gelieve het nummer van de zaak in te geven indien " +
                "u meer info wenst of indien u wenst daar een bestelling te plaatsen:");
        return choice;
    }
    public static long overzichtMenuVanGekozenZaak(long choiceZaak){
        ZaakRepo zaakRepo = new ZaakRepoImpl();
        Zaak gekozenZaak = zaakRepo.findZaak(choiceZaak);
        Collection<MenuItem> menu = gekozenZaak.getMenu();
        menu.forEach(gerecht -> System.out.println(gerecht.getId() +" : " +gerecht.getName() + " Prijs: " + gerecht.getPrice()));
        long choice = keyboard.readLong("Gelieve het nummer van het gerecht in te geven indien " +
                "indien u dit wenst te bestellen:");
        return choice;
    }

    public static long bestaandeOfNieuweKlant(){
        InputKlantMenu input= new InputKlantMenu();
        int choice = input.getBestaandOfNieuw();

            if (choice == 1) {
            klantenNummer = input.getKlantenId();

            }
            else if (choice == 2) {
         klantenNummer = nieuweKlant();
            }

        System.out.println("KlantenMenu laden....");
        return klantenNummer;
    }

    public static long nieuweKlant(){
        Input input2 = new Input();

//        TODO: klant heeft alleen nog maar naam en adres, nog telefoonnummer en email toevoegen en liefst met verificatiemethoden
        klant = new Klant(input2.getNaam(), input2.getAdres());
        KlantRepo klantRepo = new KlantRepoImpl();
        klantRepo.saveKlant(klant);
        long id = klant.getId();
        System.out.println("Welkom\n" + klant.getName() + "Uw klantennummer is: " + id +
                 "\nGelieve dit nummer goed bij te houden");
        LayOut.linePlus();
        return id;
    }
}
