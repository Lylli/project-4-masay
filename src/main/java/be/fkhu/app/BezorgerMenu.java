package be.fkhu.app;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.Status;
import be.fkhu.personen.Bezorger;
import be.fkhu.repositories.BestellingRepo;
import be.fkhu.repositories.BezorgerRepo;
import be.fkhu.repositories.implementaties.BestellingRepoImpl;
import be.fkhu.repositories.implementaties.BezorgerRepoImpl;
import be.fkhu.utilities.input.InputBezorgerMenu;
import be.fkhu.utilities.output.Keyboard;

import java.time.LocalDateTime;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class BezorgerMenu {
    private static Bezorger bezorger;
    private static long idVanDeBestelling;
    private static Keyboard keyboard;
    private static long idVanDeGeaccepteerdeBestelling;



    public static void startBezorgerMenu(){
        InputBezorgerMenu input = new InputBezorgerMenu();
        System.out.println("BezorgerMenu:");
        bezorger = findBezorger();
        System.out.println("Dit zijn de bestellingen die je momenteel geaccepteerd hebt: ");

        try{
         showAllAcceptedBestellingen();
         System.out.println("Dit zijn alle nog niet geaccepteerde bestellingen: ");
         showAllBestellingen();

        }
        catch(NullPointerException ne){
            System.out.println("Je hebt momenteel geen geaccepteerde bestellingen");
        }finally {
            int choice = input.getChoiceWatTeDoenMetBestelling();
            while (choice !=3) {
                if (choice == 1) {
                    showAllBestellingen();
                    idVanDeBestelling = acceptBestelling();
                    bestellingAccepteren(idVanDeBestelling);
                    choice = input.getChoiceWatTeDoenMetBestelling();
                } else if (choice == 2) {
                    showAllAcceptedBestellingen();
                    idVanDeGeaccepteerdeBestelling = geleverdBestelling();
                    bestellingIsGeleverd(idVanDeGeaccepteerdeBestelling);
                    choice = input.getChoiceWatTeDoenMetBestelling();
                }
            }


        }

    }

    public static void showAllBestellingen(){
        BezorgerRepo bezorgerRepo = new BezorgerRepoImpl();
        bezorgerRepo.showAllBestellingen("alleBestellingen");

    }
    public static long acceptBestelling(){
        long choice = keyboard.readLong("Gelieve het nummer van de bestelling in te geven die je wilt accepteren");
        return choice;
    }
    public static void showAllAcceptedBestellingen(){
        BezorgerRepo bezorgerRepo = new BezorgerRepoImpl();
        bezorgerRepo.showAllBestellingen("alleAccepteBestellingen");

    }
    public static long geleverdBestelling(){
        long choice = keyboard.readLong("Gelieve het nummer van de bestelling in te geven die je wilt op geleverd zetten");
        return choice;
    }

    public static Bezorger findBezorger(){
        long id = keyboard.readLong("Gelieve uw bezorgersnummer in te geven: ");
        BezorgerRepo bezorgerRepo = new BezorgerRepoImpl();
        bezorger = bezorgerRepo.findBezorger(id);
        return bezorger;
    }

    public static void bestellingAccepteren(long id){
        BestellingRepo bestellingRepo = new BestellingRepoImpl();
        Bestelling bestelling = bestellingRepo.findBestelling(id);
        bestelling.setStatus(Status.ACCEPTED);
        bestellingRepo.saveBestelling(bestelling);
        System.out.println("Bestelling is " + bestelling.getStatus());
    }

    public static void bestellingIsGeleverd(long id){
        BestellingRepo bestellingRepo = new BestellingRepoImpl();
        Bestelling bestelling = bestellingRepo.findBestelling(id);
        bestelling.setStatus(Status.GELEVERD);
        bestelling.setDateLevering(LocalDateTime.now());
        bestellingRepo.saveBestelling(bestelling);
        System.out.println("Bestelling is " + bestelling.getStatus() + " op "+ bestelling.getDateLevering());
    }
}
