package be.fkhu.app;

import be.fkhu.utilities.input.Input;
import be.fkhu.utilities.output.Keyboard;
import be.fkhu.utilities.output.LayOut;


/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class MainMenu {
    private static Keyboard keyboard;



    public static void startMenu(){
        Input input = new Input();

        System.out.println("Welkom op de Masay app\n");
        LayOut.linePlus();
        int choice = input.getChoiceKlantUitbaterOfStop();
        while (choice != 4){
            if (choice == 1){
                LayOut.linePlus();
                KlantMenu klantMenu = new KlantMenu();
                klantMenu.startKlantMenu();
                LayOut.linePlus();
                choice = input.getChoiceKlantUitbaterOfStop();

            }
            else if (choice == 2){
                LayOut.linePlus();
                UitbaterMenu uitbaterMenu= new UitbaterMenu();
                uitbaterMenu.startUitbaterMenu();
                LayOut.linePlus();
                choice = input.getChoiceKlantUitbaterOfStop();
            }
            else{
                LayOut.linePlus();
                BezorgerMenu bezorgerMenu = new BezorgerMenu();
                bezorgerMenu.startBezorgerMenu();
                LayOut.linePlus();
                choice = input.getChoiceKlantUitbaterOfStop();
            }
        }
        System.out.println("App is aan het afsluiten....");
        keyboard.closeKeyboard();

    }



}
