package be.fkhu.app;

import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.repositories.UitbaterRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.repositories.implementaties.MenuItemRepoImpl;
import be.fkhu.repositories.implementaties.UitbaterRepoImpl;
import be.fkhu.repositories.implementaties.ZaakRepoImpl;
import be.fkhu.utilities.input.Input;
import be.fkhu.utilities.input.InputUitbaterMenu;
import be.fkhu.utilities.output.Keyboard;
import be.fkhu.utilities.output.LayOut;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.StatusZaak;
import be.fkhu.zaken.Zaak;

import java.util.Collection;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class UitbaterMenu {
    private static Uitbater uitbater;
    private static long uitbaterNummer;
    private static Keyboard keyboard;
    private static int choiceUitbater;
    private static long choiceZaak;


    public static void startUitbaterMenu(){
        System.out.println("Uitbatermenu: ");
        long id = bestaandeOfNieuweUitbater();
        UitbaterRepo uitbaterRepo = new UitbaterRepoImpl();
        uitbater = uitbaterRepo.findUitbater(id);
        System.out.println("Dit zijn uw zaken met hun identiteitsnummers:");
        Collection<Zaak> zaken = uitbater.getZaken();

        if(zaken.isEmpty()){
            System.out.println("U heeft geen zaken!");
        }
        else{
            zaken.forEach(zaak -> System.out.println(zaak.getId() + " : " + zaak.getNameZaak() + " is " + zaak.getStatusZaak() + " Adres: " + zaak.getAdres() + " Openingsuren: " +
                    zaak.getOpeningsUren()));
        }
        watTeDoenMetMijnZaken();

    }

    public static void watTeDoenMetMijnZaken(){
        InputUitbaterMenu input= new InputUitbaterMenu();
        choiceUitbater = input.getChoiceWatTeDoenMetZaken();
        while (choiceUitbater != 4){
            if (choiceUitbater == 1){
                zaakToevoegen();
                choiceUitbater = input.getChoiceWatTeDoenMetZaken();
            }
            else if (choiceUitbater == 2){
                zaakUpdaten();
                choiceUitbater = input.getChoiceWatTeDoenMetZaken();
            }
            else{
                zaakVerwijderen();
                choiceUitbater = input.getChoiceWatTeDoenMetZaken();
            }
        }
    }

    public static void zaakToevoegen(){
        UitbaterRepo uitbaterRepo = new UitbaterRepoImpl();
        long id = uitbater.getId();
        InputUitbaterMenu input = new InputUitbaterMenu();
        Input inputAdres = new Input();
        Zaak zaak = new Zaak(input.getNameZaak(), inputAdres.getAdres(), input.getOpeningsuren());
        uitbaterRepo.addZaak(id, zaak);
        int choiceToevoegenGerecht = keyboard.readInt("Wenst u een gerecht toe te voegen?\n 1. Ja\n 2.Nee");
                while (choiceToevoegenGerecht == 1){
                    choiceZaak = zaak.getId();
                    gerechtToevoegen();
                    choiceToevoegenGerecht = keyboard.readInt("Wenst u een gerecht toe te voegen?\n 1. Ja\n 2.Nee");
                }
    }

    public static void zaakUpdaten(){
        InputUitbaterMenu input= new InputUitbaterMenu();
        Input inputVoorAdres = new Input();
        Collection<Zaak> zaken = uitbater.getZaken();
        zaken.forEach(zaak -> System.out.println(zaak.getId() + " : " + zaak.getNameZaak() + " is " + zaak.getStatusZaak() + " Adres: " + zaak.getAdres() + " Openingsuren: " +
                zaak.getOpeningsUren()));
        choiceZaak = keyboard.readLong("Gelieve het nummer van de zaak in te geven die u wenst aan te passen: ");

        ZaakRepo zaakRepo= new ZaakRepoImpl();
        Zaak zaakAanTePassen = zaakRepo.findZaak(choiceZaak);

        int choiceWatAanTePassen = input.getChoiceAanpassen();
        while (choiceWatAanTePassen != 5){
            if (choiceWatAanTePassen == 1){
                String name = keyboard.readLine("Wat wordt de nieuwe naam van uw zaak?");
                zaakAanTePassen.setNameZaak(name);
                zaakRepo.mergeZaak(zaakAanTePassen);
                System.out.println("De naam van de zaak is veranderd naar: " + zaakAanTePassen.getNameZaak());
                choiceWatAanTePassen = input.getChoiceAanpassen();
            }
            else if (choiceWatAanTePassen == 2){
                System.out.println("Wat wordt het nieuwe adres? Gelieve de volgende vragen in te vullen.");
                Adres adres = inputVoorAdres.getAdres();
                zaakAanTePassen.setAdres(adres);
                zaakRepo.mergeZaak(zaakAanTePassen);
                System.out.println("het adres van de zaak is veranderd naar: " + zaakAanTePassen.getAdres());
                choiceWatAanTePassen = input.getChoiceAanpassen();
            }

            else if (choiceWatAanTePassen == 3){
                String uren = keyboard.readLine("Wat worden de nieuwe openingsuren van uw zaak?");
                zaakAanTePassen.setOpeningsUren(uren);
                zaakRepo.mergeZaak(zaakAanTePassen);
                System.out.println("De openingsuren van de zaak zijn veranderd naar: " + zaakAanTePassen.getOpeningsUren());
                choiceWatAanTePassen = input.getChoiceAanpassen();
            }

            else if (choiceWatAanTePassen == 4){

                zaakAanTePassen.setStatusZaak(StatusZaak.ACTIEF);
                zaakRepo.mergeZaak(zaakAanTePassen);
                System.out.println("Zaak " + zaakAanTePassen.getNameZaak() + " Status: "+ zaakAanTePassen.getStatusZaak());
                choiceWatAanTePassen = input.getChoiceAanpassen();
            }


            else{
                int choiceMenuAanpassen = input.getChoiceMenuAanpassen();
                while (choiceMenuAanpassen != 3){
                    if (choiceMenuAanpassen == 1){
                        gerechtToevoegen();
                        zaakRepo.mergeZaak(zaakAanTePassen);
                        choiceMenuAanpassen = input.getChoiceMenuAanpassen();
                    }
                    else{
                        gerechtVerwijderen();
                        zaakRepo.mergeZaak(zaakAanTePassen);
                        choiceMenuAanpassen = input.getChoiceMenuAanpassen();
                    }
                }

                choiceWatAanTePassen = input.getChoiceAanpassen();
            }
        }

    }

    public static void gerechtToevoegen(){
        String name = keyboard.readLine("Wat wordt de naam van uw gerecht? ");
        double price = keyboard.readDouble("Wat wordt de prijs van uw gerecht?");
        MenuItem gerecht = new MenuItem(name, price);
        MenuItemRepo gerechtRepo = new MenuItemRepoImpl();
        gerechtRepo.addGerecht(choiceZaak, gerecht);
        gerechtRepo.saveGerecht(gerecht);
    }

    public static void gerechtVerwijderen(){
        ZaakRepo zaakRepo = new ZaakRepoImpl();
        Zaak zaak = zaakRepo.findZaak(choiceZaak);
        Collection<MenuItem> menu = zaak.getMenu();
        menu.forEach(gerecht -> System.out.println(gerecht.getId() +" : "+gerecht.getName()));
        long choice = keyboard.readLong("Gelieve het nummer van het gerecht in te geven die u wenst te verwijderen: ");
        MenuItemRepo menuItemRepo = new MenuItemRepoImpl();
        menuItemRepo.removeGerecht(choice);
    }

    public static void zaakVerwijderen(){
        Collection<Zaak> zaken = uitbater.getZaken();
        if (zaken.size() == 0){
            System.out.println("U heeft geen zaken om te verwijderen!");
        }else {

            zaken.forEach(zaak -> System.out.println(zaak.getId() + " : " + zaak.getNameZaak() + " is " + zaak.getStatusZaak() + " Adres: " + zaak.getAdres() + " Openingsuren: " +
                    zaak.getOpeningsUren()));
            choiceZaak = keyboard.readLong("Gelieve het nummer van de zaak in te geven die u wenst te verwijderen (opgelet, uw zaak wordt op non_actief gezet, indien u wenst de zaak uit de databank te verwijderen, gelieve contact op te nemen met uw IT-afdeling): ");

            ZaakRepo zaakRepo = new ZaakRepoImpl();
            Zaak zaakTeVerwijderen = zaakRepo.findZaak(choiceZaak);
            zaakTeVerwijderen.setStatusZaak(StatusZaak.NON_ACTIEF);
            zaakRepo.mergeZaak(zaakTeVerwijderen);
            System.out.println("Uw zaak" + zaakTeVerwijderen.getNameZaak() +" is non_actief. Toekomstige klanten zullen deze zaak niet te zien krijgen in hun menu.");
        }
    }

    public static long bestaandeOfNieuweUitbater(){
        InputUitbaterMenu input= new InputUitbaterMenu();

        int choice = input.getBestaandOfNieuw();

        if (choice == 1) {
            uitbaterNummer = input.getUitbaterId();

        }
        else if (choice == 2) {
            uitbaterNummer = nieuweUitbater();
        }

        System.out.println("UitbaterMenu laden....");
        return uitbaterNummer;
    }

    public static long nieuweUitbater(){
        Input input2 = new Input();
        uitbater = new Uitbater(input2.getNaam());
        UitbaterRepo uitbaterRepo = new UitbaterRepoImpl();
        uitbaterRepo.saveUitbater(uitbater);
        long id = uitbater.getId();
        System.out.println("Welkom\n" + uitbater.getName() + "Uw Uitbatersnummer is: " + id +
                "\nGelieve dit nummer goed bij te houden");
        LayOut.linePlus();
        return id;
    }
}
