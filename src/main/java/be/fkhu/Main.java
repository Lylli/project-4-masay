package be.fkhu;

import be.fkhu.app.MainMenu;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
public class Main {
    public static void main(String[] args) {
        MainMenu.startMenu();

    }
}
