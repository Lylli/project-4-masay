package be.fkhu.utilities.output;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class Keyboard implements Serializable {
    static Scanner keyboard = new Scanner(System.in);
//    private Keyboard keyboard2;

    public static int readInt(String text) {
        System.out.println(text);
        while (true) {
            try {
                int result = Integer.parseInt(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Gelieve een nummer in te geven.");

            }
        }
    }

    public static String readLine(String text) {
        System.out.println(text);
        while (true) {
            try {
                String line = keyboard.nextLine();
                while (line.length()>50){
                    System.out.println("Het maximum dat u kan ingeven zijn 50 tekens.");
                    line= keyboard.nextLine();
                }

                line.toLowerCase();
                return line;
            } catch (InputMismatchException ime) {
                System.out.println("Gelieve in tekst te reageren.");

            }
        }
    }
    public static long readLong(String text) {
        System.out.println(text);
        while (true) {
            try {
                long result = Long.parseLong(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Gelieve een nummer in te geven.");

            }
        }
    }
    public static double readDouble(String text) {
        System.out.println(text);
        while (true) {
            try {
                double result = Double.parseDouble(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Gelieve een nummer in te geven.");

            }
        }
    }


    public static void closeKeyboard() {
        keyboard.close();
    }

}
