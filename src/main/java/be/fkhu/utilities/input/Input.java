package be.fkhu.utilities.input;

import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.utilities.output.Keyboard;

import java.io.Serializable;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */

public class Input implements Serializable {
    Keyboard keyboard;



    public int getChoiceKlantUitbaterOfStop(){
        int choiceMenu = keyboard.readInt(
                "1. Klant\n" +
                "2. Uitbater\n" +
                "3. Bezorger\n" +
                "4. App sluiten\n");
        while (choiceMenu <1 || choiceMenu > 4){
            System.out.println("Gelieve 1, 2, 3 of 4 te kiezen.");
            choiceMenu = keyboard.readInt(
                    "1. Klant\n" +
                            "2. Uitbater\n" +
                            "3. App sluiten\n");
        }

        return choiceMenu;
    }


    public Name getNaam(){
        Name name = new Name();
        String firstName = keyboard.readLine("Wat is uw voornaam?");
        name.setFirstName(firstName);
        String lastName = keyboard.readLine("Wat is uw achternaam?");
        name.setLastName(lastName);
        return name;
    }

    public Adres getAdres(){
        Adres adres= new Adres();
        String straat = keyboard.readLine("Wat is uw straat?");
        adres.setStreet(straat);
        String nummer = keyboard.readLine("Wat is uw huisnummer en eventueel bus?");
        adres.setHouseNumber(nummer);
        int postcode = keyboard.readInt("Wat is uw postcode?");
        adres.setZipcode(postcode);
        String stad = keyboard.readLine("Wat is uw stad?");
        adres.setCity(stad);
        return adres;
    }
}
