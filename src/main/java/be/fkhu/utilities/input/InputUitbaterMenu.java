package be.fkhu.utilities.input;

import be.fkhu.utilities.output.Keyboard;

/**
 * @author Natalie Heylen
 * created on woensdag, 3/03/2021
 */
public class InputUitbaterMenu {
    Keyboard keyboard;

    public int getBestaandOfNieuw(){
        int choice1 = keyboard.readInt("Bent u\n"+
                "1. Een bestaande uitbater\n"+
                "2. Een nieuwe uitbater\n");
        while (choice1 <1 || choice1 > 3){
            System.out.println("Gelieve 1 of 2 te kiezen");
            choice1 = keyboard.readInt("Bent u\n"+
                    "1. Een bestaande uitbater\n"+
                    "2. Een nieuwe uitbater\n");
        }
        return choice1;
    }
    public long getUitbaterId(){
        long UitbaterId = keyboard.readLong("Gelieve uw uitbater-nummer in te geven.");
        return UitbaterId;
    }

    public int getChoiceWatTeDoenMetZaken(){
        int choice = keyboard.readInt("Wat wenst u te doen?\n"+
                "1.  Een zaak toevoegen\n" +
                "2.  Een zaak aanpassen\n" +
                "3.  Een zaak verwijderen\n" +
                "4.  terug te gaan naar het hoofdmenu\n");
        while (choice <1 || choice > 4){
            System.out.println("Gelieve een valide nummer te kiezen");
            choice = keyboard.readInt("Wat wenst u te doen?\n"+
                    "1.  Een zaak toevoegen\n" +
                    "2.  Een zaak aanpassen\n" +
                    "3.  Een zaak verwijderen\n" +
                    "4.  terug te gaan naar het hoofdmenu\n");}
        return choice;
    }

    public String getNameZaak(){
        String name =keyboard.readLine("Wat is de naam van uw nieuwe zaak?" );
        return name;
    }
    public String getOpeningsuren(){
        String uren =keyboard.readLine("Wat zijn de openingsuren (U kan deze gewoon intypen)?" );
        return uren;
    }

    public int getChoiceAanpassen(){
        int choice = keyboard.readInt("Wat wenst u aan te passen?\n"+
                "1. De naam\n" +
                "2. Het adres\n" +
                "3. De openingsuren\n" +
                "4. Een zaak heractiveren\n" +
                "5. De menu\n" +
                "6. Niets, ik wil terug naar het vorige scherm\n");
        while (choice <1 || choice > 6) {
            System.out.println("Gelieve een valide nummer te kiezen");
            choice = keyboard.readInt("Wat wenst u aan te passen?\n" +
                    "1. De naam\n" +
                    "2. Het adres\n" +
                    "3. De openingsuren\n" +
                    "4. Een zaak heractiveren\n" +
                    "5. De menu\n" +
                    "6. Niets, ik wil terug naar het vorige scherm\n");
        }
        return choice;
    }

    public int getChoiceMenuAanpassen(){
        int choice = keyboard.readInt("Wat wenst u te doen?\n"+
                "1. Gerecht toevoegen\n" +
                "2. Gerecht verwijderen\n" +
                "3. Terug te gaan naar de vorige menu\n");
        while (choice <1 || choice > 3) {
            System.out.println("Gelieve een valide nummer te kiezen");
            choice = keyboard.readInt("Wat wenst u te doen?\n" +
                    "1. Gerecht toevoegen\n" +
                    "2. Gerecht verwijderen\n" +
                    "3. Terug te gaan naar de vorige menu\n");
        }
        return choice;
    }

}
