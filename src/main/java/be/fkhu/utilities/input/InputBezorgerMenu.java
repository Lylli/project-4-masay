package be.fkhu.utilities.input;

import be.fkhu.utilities.output.Keyboard;

/**
 * @author Natalie Heylen
 * created on donderdag, 4/03/2021
 */
public class InputBezorgerMenu {
    Keyboard keyboard;

    public int getChoiceWatTeDoenMetBestelling() {
        int choice = keyboard.readInt("Wat wilt u doen?\n" +
                "1.  Bestelling accepteren\n" +
                "2.  Bestelling is geleverd\n" +
                "3.  teruggaan naar het hoofdmenu\n");
        while (choice < 1 || choice > 3) {
            System.out.println("Gelieve een valide nummer te kiezen");
            choice = keyboard.readInt("Wat wilt u met deze bestelling doen?\n" +
                    "1.  Bestelling accepteren\n" +
                    "2.  Bestelling is geleverd\n" +
                    "3.  teruggaan naar het hoofdmenu\n");

        }return choice;
    }
}
