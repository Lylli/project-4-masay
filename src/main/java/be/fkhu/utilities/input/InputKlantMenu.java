package be.fkhu.utilities.input;

import be.fkhu.utilities.output.Keyboard;

import java.io.Serializable;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class InputKlantMenu implements Serializable {
    Keyboard keyboard;

    public int meerBestellen(){
        int choiceMeerBestellen = keyboard.readInt("Wenst u meer te bestellen?\n"+
                "1. Ja\n"+
                "2. Nee\n");
        while (choiceMeerBestellen <1 || choiceMeerBestellen > 2){
            System.out.println("Gelieve 1 of 2 te kiezen");
            choiceMeerBestellen = keyboard.readInt("Wenst u meer te bestellen?\n"+
                    "1. Ja\n"+
                    "2. Nee\n");
        }
        return choiceMeerBestellen;
    }
    public int getBestaandOfNieuw(){
        int choice1 = keyboard.readInt("Bent u\n"+
                "1. Een bestaande klant\n"+
                "2. Een nieuwe klant\n" );
        while (choice1 <1 || choice1 > 2){
            System.out.println("Gelieve 1 of 2 te kiezen");
            choice1 = keyboard.readInt("Bent u\n"+
                    "1. Een bestaande klant\n"+
                    "2. Een nieuwe klant\n");
        }
        return choice1;
    }

    public long getKlantenId(){
        long klantId = keyboard.readLong("Gelieve uw klantennummer in te geven.");
        return klantId;
    }

    public int getChoiceZaakOfBestellingKeuzeMenu(){
        int choice = keyboard.readInt("Wenst u...\n"+
                "1. een bestelling te plaatsen?\n"+
                "2. uw bestellingen te zien?\n"+
                "3. terug te gaan naar hoofdmenu?\n");
        while (choice <1 || choice > 3) {
            System.out.println("Gelieve 1, 2 of 3 te kiezen");
            choice = keyboard.readInt("Wenst u...\n" +
                    "1. alle aangesloten zaken te zien?\n" +
                    "2. uw bestellingen te zien?\n" +
                    "3. terug te gaan naar hoofdmenu?\n");
        }
        return choice;
    }
}
