package be.fkhu.utilities.jpaHelper;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class JPAFacade implements AutoCloseable {
    private EntityManagerFactory managerFactory;
    private EntityManager manager;
    private EntityTransaction transaction;

    public JPAFacade() {
        this("course");
    }

    public JPAFacade(String persistenceUnit) {
        managerFactory = Persistence.createEntityManagerFactory(persistenceUnit);
        manager = managerFactory.createEntityManager();
    }

    public EntityManager getManager() {
        return manager;
    }

    public EntityTransaction getTransaction() {
        if (Objects.isNull(transaction)){
            transaction = manager.getTransaction();
        }
        return transaction;
    }

    public void beginTransaction() {
        transaction.begin();
    }

    public void commitTransaction() {
        transaction.commit();
    }

    public <T> T persist(T entity) {
        getTransaction().begin();
        manager.persist(entity);
        commitTransaction();
        return entity;
    }

    public <T> T merge(T entity) {
        getTransaction().begin();
        entity = manager.merge(entity);
        commitTransaction();
        return entity;
    }
    public <T> T find(Class<T> classType, long id) {
        getTransaction().begin();
        T entity = manager.find(classType, id);
        commitTransaction();
        return entity;
    }

    public <T> T findWithName(Class<T> classType, String name) {
        getTransaction().begin();
        T entity = manager.find(classType, name);
        commitTransaction();
        return entity;
    }
    public <T> List<T> executeNamedQueryListResult(String namedQuery, Class<T> tClass){
        TypedQuery<T> query= manager.createNamedQuery(namedQuery, tClass);
        return query.getResultList();
    }
    @Override
    public void close() {
        if (Objects.nonNull(manager)) {
            manager.close();
        }
        if (Objects.nonNull(managerFactory)) {
            managerFactory.close();
        }
    }
}
