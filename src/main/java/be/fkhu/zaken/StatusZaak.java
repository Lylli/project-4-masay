package be.fkhu.zaken;

/**
 * @author Natalie Heylen
 * created on donderdag, 4/03/2021
 */
public enum StatusZaak {
    ACTIEF, NON_ACTIEF;
}
