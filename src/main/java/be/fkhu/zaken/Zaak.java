package be.fkhu.zaken;

import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.Uitbater;
import be.fkhu.personen.persoonsgegevens.Adres;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
@Entity
@Table(name = "Project4_Zaken")
@NamedQuery(name = "findAllZaken", query = "select z from Zaak z where z.statusZaak = be.fkhu.zaken.StatusZaak.ACTIEF")
@NamedQuery(name = "showMenu", query = "select m from MenuItem m")
public class Zaak {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZaakID")
    private long id;

    @Column(name = "Bedrijfsnaam")
    private String nameZaak;

    @OneToOne (cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "AdresID")
    private Adres adres;


    @ManyToOne
    @JoinColumn(name = "UitbaterID")
    private Uitbater uitbater;

    @Column(name = "Openingsuren")
    private String openingsUren;

    @Column(name = "Status", columnDefinition = "ENUM('ACTIEF','NON_ACTIEF')")
    @Enumerated(EnumType.STRING)
    private StatusZaak statusZaak;


    @OneToMany (mappedBy = "zaak", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    Collection<MenuItem> menu;



    public Zaak() {
        setStatusZaak(StatusZaak.ACTIEF);
    }

    public Zaak(String nameZaak) {
        this.nameZaak = nameZaak;
        setStatusZaak(StatusZaak.ACTIEF);
    }
    public Zaak(String nameZaak, Adres adres) {
        this.nameZaak = nameZaak;
        this.adres = adres;
        setStatusZaak(StatusZaak.ACTIEF);
    }

    public Zaak(String nameZaak, Adres adres, String openingsUren) {
        this.nameZaak = nameZaak;
        this.adres = adres;
        this.openingsUren = openingsUren;
        setStatusZaak(StatusZaak.ACTIEF);
    }

    public Zaak(String nameZaak, Adres adres, String openingsUren, Collection<MenuItem> menu) {
        this.nameZaak = nameZaak;
        this.adres = adres;
        this.openingsUren = openingsUren;
        this.menu = menu;
        setStatusZaak(StatusZaak.ACTIEF);
    }



    public String getOpeningsUren() {
        return openingsUren;
    }

    public void setOpeningsUren(String openingsUren) {
        this.openingsUren = openingsUren;
    }

    public Collection<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(Collection<MenuItem> menu) {
        this.menu = menu;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getNameZaak() {
        return nameZaak;
    }

    public void setNameZaak(String nameZaak) {
        this.nameZaak = nameZaak;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    public Uitbater getUitbater() {
        return uitbater;
    }

    public void setUitbater(Uitbater uitbater) {
        this.uitbater = uitbater;
    }

    public StatusZaak getStatusZaak() {
        return statusZaak;
    }

    public void setStatusZaak(StatusZaak statusZaak) {
        this.statusZaak = statusZaak;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zaak zaak = (Zaak) o;
        return Objects.equals(nameZaak, zaak.nameZaak) && Objects.equals(adres, zaak.adres) && Objects.equals(uitbater, zaak.uitbater) &&
                Objects.equals(openingsUren, zaak.openingsUren) && statusZaak == zaak.statusZaak && Objects.equals(menu, zaak.menu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameZaak, adres, uitbater, openingsUren, statusZaak, menu);
    }

    @Override
    public String toString() {
        return "Zaak{" +

                "nameZaak='" + nameZaak + '\'' +
                ", adres=" + adres +
                ", openingsUren='" + openingsUren + '\'' +

                '}';
    }
}
