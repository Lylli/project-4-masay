package be.fkhu.zaken;


import javax.persistence.*;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
@Entity
@Table(name ="Project4_MenuItems")

public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MenuItemID")
    private long id;

    @Column(name="MenuItemNaam")
    private String name;
    @Column(name="eenheidsprijs")
    private double price;


    @ManyToOne
    @JoinTable(name = "Project4_Menu_Zaak_Koppel",
    joinColumns = @JoinColumn(name ="MenuItemID"),
    inverseJoinColumns = @JoinColumn(name = "ZaakID"))
    private Zaak zaak;




    public MenuItem() {
    }

    public MenuItem(String name, double price) {
        this.name = name;
        this.price = price;
    }


    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Zaak getZaak() {
        return zaak;
    }

    public void setZaak(Zaak zaak) {
        this.zaak = zaak;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Double.compare(menuItem.price, price) == 0 && Objects.equals(name, menuItem.name) && Objects.equals(zaak, menuItem.zaak);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, zaak);
    }

    @Override
    public String toString() {
        return "Menu_Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
