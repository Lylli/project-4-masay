package be.fkhu.repositories;

import be.fkhu.zaken.MenuItem;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface MenuItemRepo {
    void saveGerecht(MenuItem gerecht);
    MenuItem findGerecht(long id);
    void addGerecht (long id, MenuItem gerecht);
    void removeGerecht (long id);


}
