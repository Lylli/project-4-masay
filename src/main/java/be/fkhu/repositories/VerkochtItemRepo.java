package be.fkhu.repositories;


import be.fkhu.orders.VerkochtItem;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface VerkochtItemRepo {
    void saveVerkochtItem(VerkochtItem verkochtItem);
    VerkochtItem findVerkochtItem(long id);
}
