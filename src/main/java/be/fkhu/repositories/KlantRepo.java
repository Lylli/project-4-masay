package be.fkhu.repositories;

import be.fkhu.personen.Bezorger;
import be.fkhu.personen.Klant;
import be.fkhu.zaken.Zaak;

import java.util.List;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface KlantRepo {

    void saveKlant(Klant klant);
    Klant findKlant(long id);
    List<Zaak> showAllZaken(String query);
    void updateKlant(Klant klant);

}
