package be.fkhu.repositories;

import be.fkhu.zaken.Zaak;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface ZaakRepo {
    void saveZaak(Zaak zaak);
    Zaak findZaak(long id);
    void mergeZaak(Zaak zaak);


}
