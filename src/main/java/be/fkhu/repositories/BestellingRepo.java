package be.fkhu.repositories;

import be.fkhu.orders.Bestelling;
import be.fkhu.orders.VerkochtItem;


/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface BestellingRepo {
    void saveBestelling(Bestelling bestelling);
    Bestelling findBestelling(long id);
    void addVerkochtItem (long id, VerkochtItem verkochtItem);
}
