package be.fkhu.repositories;

import be.fkhu.orders.Bestelling;
import be.fkhu.personen.Bezorger;
import be.fkhu.personen.persoonsgegevens.Adres;

import java.util.List;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface BezorgerRepo {
    void saveBezorger(Bezorger bezorger);
    Bezorger findBezorger(long id);
    List<Bestelling> showAllBestellingen(String query);
}
