package be.fkhu.repositories.implementaties;

import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;

import java.util.List;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class ZaakRepoImpl implements ZaakRepo {
    JPAFacade facade;

    public ZaakRepoImpl(){
        this(new JPAFacade());
    }

    public ZaakRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveZaak(Zaak zaak) {
        facade.persist(zaak);
    }
    @Override
    public void mergeZaak(Zaak zaak) {
        facade.merge(zaak);
    }

    @Override
    public Zaak findZaak(long id) {
        Zaak zaak = facade.find(Zaak.class, id);
        return zaak;
    }




}
