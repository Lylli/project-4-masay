package be.fkhu.repositories.implementaties;

import be.fkhu.orders.Bestelling;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.personen.Bezorger;
import be.fkhu.repositories.BezorgerRepo;


import java.util.List;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class BezorgerRepoImpl implements BezorgerRepo {
    JPAFacade facade;

    public BezorgerRepoImpl(){
        this(new JPAFacade());
    }

    public BezorgerRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveBezorger(Bezorger bezorger) {
        facade.persist(bezorger);

    }

    @Override
    public Bezorger findBezorger(long id) {
        return facade.find(Bezorger.class, id);
    }

    @Override
    public List<Bestelling> showAllBestellingen(String query){
        try(JPAFacade jpaFacade = new JPAFacade()){
            List<Bestelling> bestellingen = jpaFacade.executeNamedQueryListResult(query, Bestelling.class);
            if (bestellingen.isEmpty()){
                System.out.println("Er zijn geen bestellingen beschikbaar!");
            }
            else{

            bestellingen.forEach(bestelling -> System.out.println(bestelling.getId() +" Klant: " +bestelling.getKlant().getName() +
                    " -Adres van de klant: "+ bestelling.getKlant().getAdres() +
                    " - Zaak: " + bestelling.getZaak().getNameZaak() + " - Adres van de zaak: " +bestelling.getZaak().getAdres() +
                    "Aantal gerechten: " + bestelling.getVerkochteItems().size()));}

            return bestellingen;}
    }
}
