package be.fkhu.repositories.implementaties;

import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.personen.Uitbater;
import be.fkhu.repositories.UitbaterRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;

import java.util.Collection;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class UitbaterRepoImpl implements UitbaterRepo {
    JPAFacade facade;

    public UitbaterRepoImpl(){
        this(new JPAFacade());
    }

    public UitbaterRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveUitbater(Uitbater uitbater) {
        facade.persist(uitbater);

    }

    @Override
    public Uitbater findUitbater(long id) {
        return facade.find(Uitbater.class, id);
    }

    @Override
    public void addZaak(long id, Zaak zaak) {
        Uitbater uitbater;
        uitbater = facade.find(Uitbater.class, id);

        ZaakRepo zaakRepo = new ZaakRepoImpl();
        zaak.setUitbater(uitbater);
        zaakRepo.saveZaak(zaak);
        uitbater.getZaken().add(zaak);

        System.out.println("Zaak " + zaak.getNameZaak() +" is toegevoegd aan uw zaken.");

    }

    @Override
    public void removeZaak(long id, Collection<MenuItem> menu) {
        Zaak zaak;
        zaak= facade.find(Zaak.class, id);
        zaak.getMenu().removeAll(menu);
        facade.getTransaction().begin();
        facade.getManager().remove(zaak);
        facade.getTransaction().commit();

        System.out.println("Zaak " + zaak.getNameZaak() + " is verwijderd van uw zaken.");
    }


}
