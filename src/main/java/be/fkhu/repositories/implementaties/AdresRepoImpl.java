package be.fkhu.repositories.implementaties;

import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.repositories.AdresRepo;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class AdresRepoImpl implements AdresRepo {
    JPAFacade facade;

    public AdresRepoImpl(){
        this(new JPAFacade());
    }

    public AdresRepoImpl(JPAFacade facade){
        this.facade= facade;
    }

    @Override
    public void saveAdres(Adres adres) {
        facade.merge(adres);

    }

    @Override
    public Adres findAdres(long id) {
        return facade.find(Adres.class, id);
    }
}
