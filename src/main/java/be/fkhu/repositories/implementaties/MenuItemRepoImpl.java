package be.fkhu.repositories.implementaties;

import be.fkhu.personen.Uitbater;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.repositories.MenuItemRepo;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class MenuItemRepoImpl implements MenuItemRepo {
    JPAFacade facade;

    public MenuItemRepoImpl(){
        this(new JPAFacade());
    }

    public MenuItemRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveGerecht(MenuItem gerecht) {
        facade.merge(gerecht);
    }

    @Override
    public MenuItem findGerecht(long id) {
        return facade.find(MenuItem.class, id);
    }


    @Override
    public void addGerecht(long id, MenuItem gerecht) {
        Zaak zaak;
        zaak = facade.find(Zaak.class, id);
        gerecht.setZaak(zaak);
        MenuItemRepo gerechtRepo = new MenuItemRepoImpl();

        zaak.getMenu().add(gerecht);
        zaak.setMenu(zaak.getMenu());
        gerechtRepo.saveGerecht(gerecht);

        System.out.println("Gerecht " + gerecht.getName() +" is toegevoegd aan uw menu bij zaak "+zaak.getNameZaak() +".");
    }

    @Override
    public void removeGerecht(long id) {
        MenuItem gerecht;
        gerecht = facade.find(MenuItem.class, id);
        facade.getTransaction().begin();
        facade.getManager().remove(gerecht);
        facade.getTransaction().commit();
        System.out.println("Gerecht " + gerecht.getName() + " is verwijderd.");
    }


}
