package be.fkhu.repositories.implementaties;

import be.fkhu.orders.VerkochtItem;
import be.fkhu.personen.Uitbater;
import be.fkhu.repositories.VerkochtItemRepo;
import be.fkhu.repositories.ZaakRepo;
import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.orders.Bestelling;
import be.fkhu.repositories.BestellingRepo;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class BestellingRepoImpl implements BestellingRepo {
    JPAFacade facade;

    public BestellingRepoImpl(){
        this(new JPAFacade());
    }

    public BestellingRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveBestelling(Bestelling bestelling) {
        facade.persist(bestelling);

    }

    @Override
    public Bestelling findBestelling(long id) {
        return facade.find(Bestelling.class, id);
    }

    @Override
    public void addVerkochtItem(long id, VerkochtItem verkochtItem) {
        Bestelling bestelling;
        bestelling = facade.find(Bestelling.class, id);

        VerkochtItemRepo verkochtItemRepo = new VerkochtItemRepoImpl();
        verkochtItem.setBestelling(bestelling);
        verkochtItemRepo.saveVerkochtItem(verkochtItem);
        bestelling.getVerkochteItems().add(verkochtItem);

        System.out.println("Gerecht" + verkochtItem.getGerecht() +" is toegevoegd aan uw bestelling.");
    }
}
