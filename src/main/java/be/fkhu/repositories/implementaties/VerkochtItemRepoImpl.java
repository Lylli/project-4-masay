package be.fkhu.repositories.implementaties;

import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.orders.VerkochtItem;
import be.fkhu.repositories.VerkochtItemRepo;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class VerkochtItemRepoImpl implements VerkochtItemRepo {
    JPAFacade facade;

    public VerkochtItemRepoImpl(){
        this(new JPAFacade());
    }

    public VerkochtItemRepoImpl(JPAFacade facade){
        this.facade= facade;
    }

    @Override
    public void saveVerkochtItem(VerkochtItem verkochtItem) {
        facade.persist(verkochtItem);

    }

    @Override
    public VerkochtItem findVerkochtItem(long id) {
        return facade.find(VerkochtItem.class, id);
    }
}
