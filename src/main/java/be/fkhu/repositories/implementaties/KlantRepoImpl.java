package be.fkhu.repositories.implementaties;


import be.fkhu.utilities.jpaHelper.JPAFacade;
import be.fkhu.personen.Klant;
import be.fkhu.repositories.KlantRepo;
import be.fkhu.zaken.Zaak;
import java.util.List;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public class KlantRepoImpl implements KlantRepo {
    JPAFacade facade;

    public KlantRepoImpl(){
        this(new JPAFacade());
    }

    public KlantRepoImpl(JPAFacade facade){
        this.facade= facade;
    }
    @Override
    public void saveKlant(Klant klant) {
        facade.persist(klant);

    }

    @Override
    public Klant findKlant(long id) {
        return facade.find(Klant.class, id);
    }
    @Override
    public void updateKlant(Klant klant) {
    facade.merge(klant);

}

    @Override
    public List<Zaak> showAllZaken(String query){
        try(JPAFacade jpaFacade = new JPAFacade()){
            List<Zaak> zaken = jpaFacade.executeNamedQueryListResult(query, Zaak.class);
            if (zaken.isEmpty()){
                System.out.println("Er zijn geen zaken weer te geven!");
            }
            else{
            zaken.forEach(zaak -> System.out.println(zaak.getId() +" : " +zaak.getNameZaak() +
                    " - Adres: " + zaak.getAdres() + " - Openingsuren zijn: " +zaak.getOpeningsUren()));}

            return zaken;}
    }
}
