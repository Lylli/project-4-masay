package be.fkhu.repositories;

import be.fkhu.personen.Uitbater;
import be.fkhu.zaken.MenuItem;
import be.fkhu.zaken.Zaak;

import java.util.Collection;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface UitbaterRepo {
    void saveUitbater(Uitbater uitbater);
    Uitbater findUitbater(long id);
    void addZaak(long id, Zaak zaak);
    void removeZaak(long id, Collection<MenuItem> menu);
}
