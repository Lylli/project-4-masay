package be.fkhu.repositories;

import be.fkhu.personen.persoonsgegevens.Adres;

/**
 * @author Natalie Heylen
 * created on dinsdag, 2/03/2021
 */
public interface AdresRepo {
    void saveAdres(Adres adres);
    Adres findAdres(long id);
}
