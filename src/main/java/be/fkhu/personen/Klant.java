package be.fkhu.personen;

import be.fkhu.orders.Bestelling;
import be.fkhu.personen.persoonsgegevens.Adres;
import be.fkhu.personen.persoonsgegevens.Name;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */

@Entity
@Table(name = "Project4_Klanten")


public class Klant {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "KlantID")
    private long id;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="AdresID")
    private Adres adres;
    @Embedded
    private Name name;
    @OneToMany(mappedBy = "klant", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    Collection<Bestelling> bestellingen;

    public Klant() {
    }

    public Klant(Name name) {
        this.name = name;
    }

    public Klant(Name name, Adres adres) {
        this.adres = adres;
        this.name = name;
        this.bestellingen = new ArrayList<>();

    }

    public Klant(Name name, Adres adres,  Collection<Bestelling> bestellingen) {
        this.adres = adres;
        this.name = name;
        this.bestellingen = bestellingen;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Collection<Bestelling> getBestellingen() {
        return bestellingen;
    }

    public void setBestellingen(Collection<Bestelling> bestellingen) {
        this.bestellingen = bestellingen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klant klant = (Klant) o;
        return Objects.equals(adres, klant.adres) && Objects.equals(name, klant.name) && Objects.equals(bestellingen, klant.bestellingen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adres, name, bestellingen);
    }

    @Override
    public String toString() {
        return "Klant{" +
                "id=" + id +
                ", adres=" + adres +
                ", name=" + name +

                '}';
    }
}
