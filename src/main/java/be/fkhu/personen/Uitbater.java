package be.fkhu.personen;

import be.fkhu.personen.persoonsgegevens.Name;
import be.fkhu.zaken.Zaak;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
@Entity
@Table(name= "Project4_Uitbaters")
public class Uitbater {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="UitbaterID")
    private long id;
    @Embedded
    private Name name;

    @OneToMany (mappedBy = "uitbater")
    Collection<Zaak> zaken;

    public Uitbater() {
    }

    public Uitbater(Name name) {
        this.name = name;
    }

    public Uitbater(Name name, Collection<Zaak> zaken) {
        this.name = name;
        this.zaken = zaken;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Collection<Zaak> getZaken() {
        return zaken;
    }

    public void setZaken(Collection<Zaak> zaken) {
        this.zaken = zaken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Uitbater uitbater = (Uitbater) o;
        return Objects.equals(name, uitbater.name) && Objects.equals(zaken, uitbater.zaken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, zaken);
    }

    @Override
    public String toString() {
        return "Uitbater{" +
                "id=" + id +
                ", name=" + name +
                ", zaken=" + zaken +
                '}';
    }

}
