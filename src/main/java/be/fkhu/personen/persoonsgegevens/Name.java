package be.fkhu.personen.persoonsgegevens;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
@Embeddable
public class Name {
    @Column(name = "Voornaam")
    private String firstName;
    @Column(name = "Achternaam")
    private String lastName;

    public Name(){
    }

    public Name(String firstName) {
        this.firstName = firstName;
    }

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(firstName, name.firstName) && Objects.equals(lastName, name.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return "Persoon{" +
                "Voornaam='" + firstName + '\'' +
                ", Achternaam='" + lastName + '\'' +
                '}';
    }

}
