package be.fkhu.personen.persoonsgegevens;

import be.fkhu.personen.Klant;
import be.fkhu.zaken.Zaak;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */
@Entity
@Table(name = "Project4_Adressen")
public class Adres {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AdresID")
    private long id;

    @OneToMany (mappedBy = "adres", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Collection<Klant> klanten;

    @OneToOne(mappedBy = "adres", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Zaak zaak;

    @Column(name = "Straat")
    private String street;
    @Column(name = "Nummer")
    private String houseNumber;

    @Column(name = "Postcode")
    private int zipcode;
    @Column(name = "Gemeente")
    private String city;

    public Adres(){

    }

    public Adres(String street, String houseNumber, int zipcode, String city) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipcode = zipcode;
        this.city = city;
    }



    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public Collection<Klant> getKlanten() {
        return klanten;
    }

    public void setKlanten(Collection<Klant> klanten) {
        this.klanten = klanten;
    }

    public Zaak getZaak() {
        return zaak;
    }

    public void setZaak(Zaak zaak) {
        this.zaak = zaak;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }


    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adres adres = (Adres) o;
        return zipcode == adres.zipcode && Objects.equals(klanten, adres.klanten) && Objects.equals(zaak, adres.zaak) &&
                Objects.equals(street, adres.street) && Objects.equals(houseNumber, adres.houseNumber) && Objects.equals(city, adres.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(klanten, zaak, street, houseNumber, zipcode, city);
    }

    @Override
    public String toString() {
        return "Adres{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", houseNumber=" + houseNumber +
                ", zipcode=" + zipcode +
                ", city='" + city + '\'' +
                '}';
    }
}
