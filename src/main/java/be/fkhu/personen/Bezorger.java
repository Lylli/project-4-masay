package be.fkhu.personen;

import be.fkhu.orders.Bestelling;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Natalie Heylen
 * created on maandag, 1/03/2021
 */

@Entity
@Table(name = "Project4_Bezorgers")
public class Bezorger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BezorgerID")
    private long id;

    @Column(name = "Voornaam")
    private String name;

    @Column(name = "Vervoersmiddel")
    private String vervoersmiddel;

    @OneToOne (mappedBy = "bezorger", cascade = CascadeType.PERSIST)
    private Bestelling bestelling;




    public Bezorger() {
    }

    public Bezorger(String name) {
        this.name = name;
    }
    public Bezorger(String name, String vervoersmiddel) {
        this.name = name;
        this.vervoersmiddel = vervoersmiddel;
    }

    public Bezorger(String name, String vervoersmiddel, Bestelling bestelling) {
        this.name = name;
        this.vervoersmiddel = vervoersmiddel;
        this.bestelling = bestelling;
    }




    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVervoersmiddel() {
        return vervoersmiddel;
    }

    public void setVervoersmiddel(String vervoersmiddel) {
        this.vervoersmiddel = vervoersmiddel;
    }

    public Bestelling getBestelling() {
        return bestelling;
    }

    public void setBestelling(Bestelling bestelling) {
        this.bestelling = bestelling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bezorger bezorger = (Bezorger) o;
        return Objects.equals(name, bezorger.name) && Objects.equals(vervoersmiddel, bezorger.vervoersmiddel)
                && Objects.equals(bestelling, bezorger.bestelling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, vervoersmiddel, bestelling);
    }

    @Override
    public String toString() {
        return "Bezorger{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", vervoersmiddel='" + vervoersmiddel + '\'' +
                ", bestelling=" + bestelling +
                '}';
    }
}
